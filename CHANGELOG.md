# Changelog

This is a list of changes of both the [ct CLI application](./README.md) and the backend [ct library](./crates/ct-tracker-lib/README.md).

## v0.1.0

### CLI

- Subcommands interfacing with the library
  - `delete`
    - Delete one project (by name) or all projects (using the `--all` flag) from disk
    - Additionally, the `--no-confirm` flag can be set, so `ct` doesn't ask for confirmation
  - `list`
    - List all projects stored on this PC
    - Optional `-s` flag, to also list all the sessions in the projects
    - Optional arguments (`--fmt-date` and `--fmt-time`) for overriding the format strings using [these](https://docs.rs/chrono/0.4.19/chrono/format/strftime/index.html) specifiers
  - `report`
    - Report the time of a project
    - So far need the `-p` flag
    - Optionally the project name can be ommited, if there is one being tracked
    - Optional arguments (`--fmt-date` and `--fmt-time`) for overriding the format strings using [these](https://docs.rs/chrono/0.4.19/chrono/format/strftime/index.html) specifiers
  - `start`
    - Start tracking a project
    - So far needs the `-p` flag
  - `stop`
    - Stop tracking a project
    - So far needs the `-p` flag
    - Optionally the project name can be ommited, if there is one being tracked
  - `config`
    - Not used (yet)
  - `test`
    - Only in debug builds
    - Used to test some functionality

### library

- Project based time tracking
  - Load a project from disk
  - List all the projects on disk
  - Delete one (or all) project(s) from disk
  - Start tracking a project
  - Stop tracking a project
- Keeping track of the last started task

## v0.1.1

### CLI

- A `running` subcommand, which can be used to check if there is a project currently running
- A color choice argument (`ct -c=<your choice> <command>`)
  - This was present in v0.1.0 as well, but didn't do anything

### library

None
