use super::super::ct_fs::default_path;
use super::errors;
use super::persistent;
use super::project_path;

use glob::glob;
use std::path::PathBuf;

/// Delete all projects on this PC
pub fn all(_print_progress: bool) -> errors::CtResult<()> {
    let time_path = default_path::time_path()?;
    let paths: Vec<PathBuf> = glob(format!("{}/*.json", time_path.to_str().unwrap()).as_str())
        .unwrap()
        .filter(|p| match p {
            Ok(_) => true,
            Err(e) => {
                bunt::eprintln!("{$red}GlobError: {:?}{/$}", e);
                false
            }
        })
        .map(|p| p.unwrap())
        .collect();
    for p in paths {
        // TODO v0.2.0 -> somehow add ability to print format -> Return iterator or something
        // if print_progress {
        //     if let Ok(project) = load_proj(&p) {
        //         bunt::println!(
        //             "Deleting project {[blue]}... (Time spent: {})",
        //             project.name(),
        //             DurationWrapper::wrap(project.duration())
        //         );
        //     }
        // }
        std::fs::remove_file(p)?;
    }
    persistent::clear_store()?;
    Ok(())
}

pub fn named(name: &str) -> errors::CtResult<()> {
    std::fs::remove_file(project_path(name)?)?;
    persistent::delete_stored_if(name)?;
    Ok(())
}
