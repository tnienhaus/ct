use super::{errors, persistent, project_path, Project};

use std::{
    fs::OpenOptions,
    io::Write,
    ops::{Deref, DerefMut},
    path::PathBuf,
};

// Some RAII sugar
pub struct ProjectFrame {
    project: Project,
}

impl Deref for ProjectFrame {
    type Target = Project;

    fn deref(&self) -> &Self::Target {
        &self.project
    }
}

impl DerefMut for ProjectFrame {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.project
    }
}

impl Drop for ProjectFrame {
    fn drop(&mut self) {
        // TODO v0.2.0 -> Dirty flag, to prevent the saving of an unchanged project
        if let Err(e) = self.save() {
            eprintln!("Error while saving! {:?}", e);
        }
    }
}

impl ProjectFrame {
    pub fn load_or_create_from_name(name: &str) -> errors::CtResult<ProjectFrame> {
        let path = project_path(name)?;
        if path.exists() {
            return ProjectFrame::load(&path);
        }
        let frame = ProjectFrame {
            project: Project::create(name)?,
        };
        frame.save()?;
        Ok(frame)
    }

    fn load_persistent_and_stop() -> errors::CtResult<()> {
        if let Some(name) = persistent::has_project()? {
            let path = project_path(name.as_str())?;
            // has_project makes sure that the peoject returned exists, so we can simply unwrap
            let mut proj = ProjectFrame::load(&path)?;
            // Should also deregister from store -> project.stop() doesn't do that
            proj.stop()?;
        }
        Ok(())
    }

    pub fn load_from_name(name: &str) -> errors::CtResult<ProjectFrame> {
        let path = project_path(name)?;
        ProjectFrame::load(&path)
    }

    pub(crate) fn load(path: &PathBuf) -> errors::CtResult<ProjectFrame> {
        if path.exists() {
            assert!(path.is_file());
            // Project already exists: Try to load it and return a new frame
            let json = std::fs::read_to_string(&path)?;

            Ok(ProjectFrame {
                project: Project::from_json(json.as_str())?,
            })
        } else {
            Err(errors::CtError::Own("Project does not exist!"))
        }
    }

    fn save(&self) -> errors::CtResult<()> {
        let file = project_path(self.name())?;
        let mut file = OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(&file)?;
        file.write_all(self.project.json(true)?.as_bytes())?;
        Ok(())
    }

    pub fn start(&mut self) -> errors::CtResult<()> {
        ProjectFrame::load_persistent_and_stop()?;
        self.project.start();
        persistent::register(self.project.name().as_str())?;
        Ok(())
    }

    pub fn stop(&mut self) -> errors::CtResult<()> {
        // Maybe assert that this project is the running one
        persistent::deregister(Some(self.project.name().as_str()))?;
        self.project.stop();
        Ok(())
    }
}
