use super::super::ct_fs::default_path;
use super::errors;
use super::Project;
use glob::glob;

pub fn all() -> errors::CtResult<Vec<errors::CtResult<Project>>> {
    let time_path = default_path::time_path()?;
    Ok(
        glob(format!("{}/*.json", time_path.to_str().unwrap()).as_str())
            .unwrap()
            .map(|path| Project::load(&path?))
            .collect(),
    )
}
