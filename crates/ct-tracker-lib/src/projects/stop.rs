use super::errors;
use super::persistent;
use super::ProjectFrame;

/// Stop every project that might be stored in the persistant store, except the one specified
///
/// # Returns
///
/// Returns `Some(name)` with the name of the stopped project,
/// if one was stopped, otherwise it returns `None`
pub fn stored_except(name: &str) -> errors::CtResult<Option<String>> {
    if let Some(stored) = persistent::has_project()? {
        if name == stored {
            // Still return the name, so you can check if your project was already stored
            Ok(Some(stored))
        } else {
            let mut proj = ProjectFrame::load_from_name(stored.as_str())?;
            proj.stop()?;
            // The project that has been stopped is returned
            Ok(Some(stored))
        }
    } else {
        Ok(None)
    }
}
