mod ct_fs;
pub mod errors;
pub mod projects;

pub fn init() -> errors::CtResult<()> {
    // TODO some sort of log level -> v0.2.0
    let p = ct_fs::default_path::conf_path()?;
    if ct_fs::create_path_if_missing(&p).unwrap() {
        println!("Created config path");
    }
    let p = ct_fs::default_path::time_path()?;
    if ct_fs::create_path_if_missing(&p).unwrap() {
        println!("Created time tracking path");
    }

    projects::persistent::validate()
}
