use std::path::PathBuf;

use super::ct_fs::default_path as dp;
use super::errors;

pub mod persistent;

pub mod delete;
pub mod list;
pub mod project_frame;
pub mod stop;
pub use project_frame::ProjectFrame;
pub mod project;
pub use project::Project;

pub(crate) fn project_path(name: &str) -> errors::CtResult<PathBuf> {
    let mut path = dp::time_path()?;
    path.push(format!("{}{}", name, ".json"));
    Ok(path)
}

pub fn has(name: &str) -> errors::CtResult<bool> {
    let path = project_path(name)?;
    // TODO c0.2.0 or later this is a naive approach
    //      -> it does not take empty or malformed files into account
    Ok(path.exists())
}
