use std::fmt::{Debug, Display};

pub use app_dirs::AppDirsError;
pub use glob::{GlobError, GlobResult};
pub use serde_json::{Error as JsonError, Result as JsonResult};
pub use std::io::{Error as IoError, Result as IoResult};

pub enum CtError {
    IoError(IoError),
    JsonError(JsonError),
    ADError(AppDirsError),
    GlobError(GlobError),
    Own(&'static str),
}

impl From<IoError> for CtError {
    fn from(e: IoError) -> Self {
        CtError::IoError(e)
    }
}

impl From<JsonError> for CtError {
    fn from(e: JsonError) -> Self {
        CtError::JsonError(e)
    }
}

impl From<AppDirsError> for CtError {
    fn from(e: AppDirsError) -> Self {
        CtError::ADError(e)
    }
}

impl From<GlobError> for CtError {
    fn from(e: GlobError) -> Self {
        CtError::GlobError(e)
    }
}

impl From<&'static str> for CtError {
    fn from(msg: &'static str) -> Self {
        CtError::Own(msg)
    }
}

impl Debug for CtError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            CtError::IoError(e) => write!(f, "IO Error: {}", e),
            CtError::JsonError(e) => write!(f, "JSON Error: {}", e),
            CtError::ADError(e) => write!(f, "Ad Error: {}", e),
            CtError::GlobError(e) => write!(f, "Glob Error: {}", e),
            CtError::Own(s) => write!(f, "CT Error: {}", s),
        }
    }
}

impl Display for CtError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            CtError::IoError(e) => write!(f, "IO Error: {}", e),
            CtError::JsonError(e) => write!(f, "JSON Error: {}", e),
            CtError::ADError(e) => write!(f, "Ad Error: {}", e),
            CtError::GlobError(e) => write!(f, "Glob Error: {}", e),
            CtError::Own(s) => write!(f, "CT Error: {}", s),
        }
    }
}

pub type CtResult<T> = Result<T, CtError>;
pub type ADResult<T> = Result<T, app_dirs::AppDirsError>;
