pub mod default_path;

use super::errors;
use std::fs;
use std::path::PathBuf;

/// Create a path if it doesn't exist yet
///
/// # Returns
///
/// Returns, whether a new directory has been created (true if yes)
/// Also passes along any error that fs::create_dir_all may have encountered
pub fn create_path_if_missing(path: &PathBuf) -> errors::CtResult<bool> {
    if path.exists() {
        Ok(false)
    } else {
        fs::create_dir_all(path)?;
        Ok(true)
    }
}
