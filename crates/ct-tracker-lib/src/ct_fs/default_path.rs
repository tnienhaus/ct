use super::errors;

#[cfg(not(debug_assertions))]
use app_dirs::*;
use std::path::PathBuf;

#[cfg(not(debug_assertions))]
const APP_INFO: AppInfo = AppInfo {
    name: "ct",
    author: "tnienhaus",
};
#[cfg(not(debug_assertions))]
const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");

#[cfg(not(debug_assertions))]
fn path(folder: &str, version: bool) -> errors::CtResult<PathBuf> {
    let folder = if version {
        format!("/{}/{}/", VERSION.unwrap_or("unknown"), folder)
    } else {
        format!("/{}/", folder)
    };
    get_app_dir(AppDataType::UserConfig, &APP_INFO, folder.as_str()).map_err(|e| e.into())
}

#[cfg(not(debug_assertions))]
pub fn conf_path() -> errors::CtResult<PathBuf> {
    path("/config/", false)
}

#[cfg(debug_assertions)]
pub fn conf_path() -> errors::CtResult<PathBuf> {
    Ok(PathBuf::from("./debug/config/"))
}

#[cfg(not(debug_assertions))]
pub fn time_path() -> errors::CtResult<PathBuf> {
    path("/time/", true)
}

#[cfg(debug_assertions)]
pub fn time_path() -> errors::CtResult<PathBuf> {
    Ok(PathBuf::from("./debug/time/"))
}
