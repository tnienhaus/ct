# ct-tracker-lib

[![ct-tracker-lib on crates.io](https://img.shields.io/crates/v/ct-tracker-lib)](https://crates.io/crates/ct-tracker-lib)

The meat of the ct time tracking tool. The ct-tracker binary/crate is only a CLI frontend to this library, as it itself implements all the tracking etc.
