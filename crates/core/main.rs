mod cli;
mod log;
use error_handling::CtUnwrap;
mod error_handling;

fn main() {
    let opt = cli::parse();
    log::init(opt.color());

    ct::init().ct_unwrap();

    if let Some(cmd) = opt.subcommand() {
        match cmd {
            cli::SubCommand::Config(_) => {
                // TODO for v0.2.0
                // Use this for config color value
                // Choosing a different color choice, just to show something `println`
                // can't do.
                // let mut stdout = StandardStream::stdout(ColorChoice::Always);
                bunt::writeln!(
                    log::out(),
                    "{$red}Not implemented!{/$}\n\
                {$yellow}A config and config command will probably be implemented in v0.2.0.{/$}\n\
                See the roadmap for more information."
                )
                .ct_unwrap();
            }
            cli::SubCommand::Start(cmd) => cmd.execute(),
            cli::SubCommand::Stop(cmd) => cmd.execute(),
            cli::SubCommand::List(cmd) => cmd.execute(),
            cli::SubCommand::Delete(cmd) => cmd.execute(),
            cli::SubCommand::Report(cmd) => cmd.execute(),
            cli::SubCommand::Running(cmd) => cmd.execute(),
            #[cfg(debug_assertions)]
            cli::SubCommand::Test => test(),
        }
    }
}

#[cfg(debug_assertions)]
fn test() {
    error_handling::handle_error(&ct::errors::CtError::Own(
        "Test method is currently not used!",
    ));
}
