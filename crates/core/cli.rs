use atty::Stream;
use clap::Clap;

use super::log;

pub mod opt;
pub use opt::*;
pub mod subcommand;
pub use subcommand::*;

pub mod delete;
pub mod list;
pub mod report;
pub mod running;
pub mod start;
pub mod stop;

pub mod fmt;
pub use fmt::*;

pub mod subopts {
    pub use super::delete;
    pub use super::list;
    pub use super::report;
    pub use super::running;
    pub use super::start;
    pub use super::stop;
}

pub mod defaults {
    pub mod fmt {
        // --fmt-date="%m.%d.%Y %Z %z" --fmt-time="%I:%M%p"
        // --fmt-date="%F (day %-j) %Z %:z"
        pub const FMT_STR_DATE: &str = "%d.%m.%Y %Z";
        pub const FMT_STR_TIME: &str = "%H:%M:%S %Z";
    }
}

mod session_util;

pub fn parse() -> Opt {
    Opt::parse()
}

#[allow(dead_code)]
pub fn out_is_tty() -> bool {
    atty::is(Stream::Stdout)
}

#[allow(dead_code)]
pub fn in_is_tty() -> bool {
    atty::is(Stream::Stdin)
}
