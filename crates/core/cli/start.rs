use super::fmt::*;
use super::log;
use clap::Clap;
use ct::projects::{Project, ProjectFrame};

use crate::error_handling::CtUnwrap;

enum OpMode<'a> {
    Project(&'a str),
    Task(&'a str),
}

#[derive(Clap, Debug)]
pub struct Opts {
    #[clap(short, long)]
    /// Is this a project?
    project: bool,

    /// Whats the name of the task/project
    task_name: String,
}

impl Opts {
    fn task_name(&self) -> &String {
        &self.task_name
    }

    fn is_proj(&self) -> bool {
        self.project
    }

    fn op_mode(&self) -> OpMode {
        if self.is_proj() {
            OpMode::Project(self.task_name())
        } else {
            OpMode::Task(self.task_name())
        }
    }
}

impl Opts {
    pub fn execute(&self) {
        match self.op_mode() {
            OpMode::Project(project) => {
                start_project(project);
            }
            OpMode::Task(_) => {
                bunt::writeln!(
                    log::err(),
                    "{$red}Error: Not implemented!{/$}\n\
                Task based tracking will likely come in v0.2.0"
                )
                .ct_unwrap();
            }
        }
    }
}

fn start_project(name: &str) {
    let mut project = match ProjectFrame::load_or_create_from_name(name) {
        Err(e) => {
            bunt::writeln!(
                log::err(),
                "{$red}Could not load project.{/$}\n\
            Reason: {}",
                e
            )
            .ct_unwrap();
            return;
        }
        Ok(p) => p,
    };
    if handle_stopping_stored_task(name, &project) {
        return;
    }
    bunt::writeln!(
        log::out(),
        "{$green}Start tracking project {[blue]}{/$}",
        project.name()
    )
    .ct_unwrap();
    bunt::writeln!(
        log::out(),
        "Time already spent on {[blue]}: {[green]}",
        project.name(),
        project.duration().wrap_into()
    )
    .ct_unwrap();
    project.start().ct_unwrap();
}

fn handle_stopping_stored_task(except: &str, project: &Project) -> bool {
    let stop_res = match ct::projects::stop::stored_except(except) {
        Ok(r) => r,
        Err(e) => {
            bunt::writeln!(
                log::err(),
                "{$red}Error{/$}: Could not stop the stored task.\nReason: {}",
                e
            )
            .ct_unwrap();
            return false;
        }
    };
    match stop_res {
        Some(name) if name == except => {
            bunt::writeln!(
                log::out(),
                "{$green}Project {[blue]} is already being tracked.{/$}",
                name
            )
            .ct_unwrap();
            bunt::writeln!(
                log::out(),
                "Time already spent on {[blue]}: {[green]}",
                project.name(),
                project.duration().wrap_into()
            )
            .ct_unwrap();
            true
        }
        Some(name) => {
            bunt::writeln!(
                log::out(),
                "Project {[blue]} was being tracked and has been stopped.",
                name
            )
            .ct_unwrap();
            false
        }
        _ => false,
    }
}
