use clap::Clap;

use super::subopts::*;

#[derive(Clap, Debug)]
pub enum SubCommand {
    // TODO option to prevent running task from being stopped
    /// Start tracking a task/project
    Start(start::Opts),
    // TODO option of making project/task optional, so it automatically stops the running one
    /// Stop tracking a task/project
    Stop(stop::Opts),
    /// List all projects for this version of ct
    List(list::Opts),
    /// Delete a project (or all)
    Delete(delete::Opts),
    /// Report time of a project/task
    Report(report::Opts),
    /// Check if there is a project/task being tracked
    Running(running::Opts),
    /// Set config values (not used yet)
    Config(ConfigOpts),
    #[cfg(debug_assertions)]
    Test,
}

#[derive(Clap, Debug)]
#[clap(name = "config")]
pub struct ConfigOpts {
    // TODO maybe consider making this a subcommand?
    #[clap(short = 'o', long = "open")]
    /// Open the config in the specified program
    editor: Option<String>,
}

impl ConfigOpts {
    #[allow(dead_code)]
    pub fn editor(&self) -> &Option<String> {
        &self.editor
    }
}
