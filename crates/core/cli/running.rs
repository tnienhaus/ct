use clap::Clap;

use super::log;
use super::Wrappable;
use crate::error_handling::CtUnwrap;

#[derive(Clap, Debug)]
pub struct Opts {
    #[clap(short, long)]
    /// Enable verbose output. If enabled, some information about the tracked project/task is also emitted
    verbose: bool,
}

impl Opts {
    fn verbose(&self) -> bool {
        self.verbose
    }
}

impl Opts {
    pub fn execute(&self) {
        if let Some(project) = ct::projects::persistent::has_project().ct_unwrap() {
            bunt::writeln!(
                log::out(),
                "The project/task {[blue]} is currently being tracked",
                project
            )
            .ct_unwrap();
            if self.verbose() {
                let p = ct::projects::Project::load_from_name(project.as_str()).ct_unwrap();
                bunt::writeln!(
                    log::out(),
                    "Time spent on {[blue]}: {[green+bold+italic]}",
                    project,
                    p.duration().wrap_into()
                )
                .ct_unwrap();
                bunt::writeln!(
                    log::out(),
                    "Sessions spent working on project: {[yellow]}",
                    p.session_count()
                )
                .ct_unwrap();
            }
        } else {
            println!("There is currently no project/task being tracked (that ct is aware of)");
        }
    }
}
