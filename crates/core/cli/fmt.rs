pub mod duration;
pub use duration::*;

pub struct FmtWrapper<T>(T);

pub trait Wrappable {
    fn wrap_into(self) -> FmtWrapper<Self>
    where
        Self: Sized,
    {
        FmtWrapper(self)
    }

    fn wrap(&self) -> FmtWrapper<&Self> {
        FmtWrapper(&self)
    }
}
