use super::log;
use super::Wrappable;
use crate::error_handling::CtUnwrap;
use ct::projects::project::Session;

pub fn print_session(s: &Session, fmt_time: &str, fmt_date: &str) {
    if s.is_open() {
        print_open_session(s, fmt_time, fmt_date);
    } else {
        print_closed_session(s, fmt_time, fmt_date);
    }
}

fn print_open_session(s: &Session, fmt_time: &str, fmt_date: &str) {
    bunt::writeln!(
        log::out(),
        " -> Session started on {[green]} (currently open)",
        s.start_time().format(fmt_date)
    )
    .ct_unwrap();
    bunt::writeln!(
        log::out(),
        "   -> Started: {[green]}",
        s.start_time().format(fmt_time)
    )
    .ct_unwrap();
    bunt::writeln!(
        log::out(),
        "   -> Time spent so far: {[green+italic]}",
        s.timespan().wrap_into()
    )
    .ct_unwrap();
}

fn print_closed_session(s: &Session, fmt_time: &str, fmt_date: &str) {
    let start = s.start_time();
    // Here we can safely unwrap, because end_time is only None if the session is open
    let end = s.end_time().unwrap();

    if start.date() == end.date() {
        // TODO different date formats -> config v0.2.0
        bunt::writeln!(
            log::out(),
            " -> Session on {[green]}",
            start.format(fmt_date)
        )
        .ct_unwrap();
        bunt::writeln!(
            log::out(),
            "   -> Started: {[green]}",
            start.format(fmt_time)
        )
        .ct_unwrap();
        bunt::writeln!(log::out(), "   -> Ended:   {[green]}", end.format(fmt_time)).ct_unwrap()
    } else {
        bunt::writeln!(
            log::out(),
            " -> Session from {[green]} to {[green]}",
            start.format(fmt_date),
            end.format(fmt_date)
        )
        .ct_unwrap();
        bunt::writeln!(
            log::out(),
            "   -> Started: {[green]} ({[green]})",
            start.format(fmt_time),
            start.format(fmt_date)
        )
        .ct_unwrap();
        bunt::writeln!(
            log::out(),
            "   -> Ended:   {[green]} ({[green]})",
            end.format(fmt_time),
            end.format(fmt_date)
        )
        .ct_unwrap()
    }
    bunt::writeln!(
        log::out(),
        "   -> Time spent: {[green+italic]}",
        s.timespan().wrap_into()
    )
    .ct_unwrap();
}
