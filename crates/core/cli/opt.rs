use crate::cli::subcommand::*;
use bunt::termcolor::ColorChoice;
use clap::Clap;

#[derive(Clap, Debug)]
#[clap(name = "ct", version = option_env!("CARGO_PKG_VERSION").unwrap_or("UNKOWN"), author = "Tobias Nienhaus")]
/// A simple, light-weight time tracking tool for the command line
pub struct Opt {
    #[clap(short, long, parse(from_occurrences))]
    /// Set the verbosity. (Currently unused)
    verbose: u8,

    #[clap(short, long, default_value = "auto")]
    /// Set color output (accepted: 'always', 'ansi', 'auto'; anything else defaults to 'never')
    color: String,

    #[clap(subcommand)]
    subcmd: Option<SubCommand>,
}

impl Opt {
    #[allow(dead_code)]
    pub fn color(&self) -> ColorChoice {
        // TODO different one for stderr -> might not be tty
        match self.color.as_str() {
            "always" => ColorChoice::Always,
            "ansi" => ColorChoice::AlwaysAnsi,
            "auto" => {
                if crate::cli::out_is_tty() {
                    ColorChoice::Auto
                } else {
                    ColorChoice::Never
                }
            }
            _ => ColorChoice::Never,
        }
    }

    pub fn subcommand(&self) -> &Option<SubCommand> {
        &self.subcmd
    }

    #[allow(dead_code)]
    pub fn verbosity(&self) -> u8 {
        self.verbose
    }
}
