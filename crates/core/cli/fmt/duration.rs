use chrono::Duration;
use std::fmt::Display;

use super::FmtWrapper;
use super::Wrappable;

impl Wrappable for Duration {}

impl Display for FmtWrapper<Duration> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}h{}m{}s",
            self.0.num_hours(),
            self.0.num_minutes() % 60,
            self.0.num_seconds() % 60
        )
    }
}
