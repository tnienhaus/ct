use crate::error_handling::CtUnwrap;

use super::defaults::fmt as def_fmt;
use super::fmt::*;
use super::log;
use super::session_util::*;
use clap::Clap;
use ct::projects::Project;

#[derive(Clap, Debug)]
pub struct Opts {
    // TODO also for a task of a project -> v0.2.0
    #[clap(short, long)]
    /// Should all sessions be listed as well?
    sessions: bool,

    #[clap(long)]
    fmt_date: Option<String>,

    #[clap(long)]
    fmt_time: Option<String>,
}

impl Opts {
    /// Get a reference to the list opts's sessions.
    fn print_sessions(&self) -> bool {
        self.sessions
    }

    /// Get a reference to the list opts's fmt date.
    fn fmt_date(&self) -> &str {
        if let Some(s) = &self.fmt_date {
            s.as_str()
        } else {
            def_fmt::FMT_STR_DATE
        }
        // &self.fmt_date.as_str()
    }

    /// Get a reference to the list opts's fmt time.
    fn fmt_time(&self) -> &str {
        if let Some(s) = &self.fmt_time {
            s.as_str()
        } else {
            def_fmt::FMT_STR_TIME
        }
    }
}

impl Opts {
    pub fn execute(&self) {
        let projects: Vec<_> = ct::projects::list::all()
            .ct_unwrap()
            .into_iter()
            .map(|s| s.ct_unwrap())
            .collect();
        let longest_name = get_longest_name_len(&projects);
        for p in projects.iter() {
            print_project(
                p,
                longest_name,
                self.print_sessions(),
                self.fmt_date(),
                self.fmt_time(),
            )
        }
    }
}

fn get_longest_name_len(projects: &[Project]) -> usize {
    projects.iter().fold(0, |acc, x| {
        if x.name().len() > acc {
            x.name().len()
        } else {
            acc
        }
    })
}

fn print_project(
    p: &Project,
    longest_name: usize,
    print_sessions: bool,
    fmt_date: &str,
    fmt_time: &str,
) {
    bunt::writeln!(
        log::out(),
        "- {[blue]} {}{}{[green+bold+italic]} Sessions: {[yellow]} Running? {[yellow]}",
        p.name(),
        "-".repeat(longest_name - p.name().len() + 1),
        "> Time spent: ", // bunt has a stroke with the < in front
        p.duration().wrap_into(),
        p.session_count(),
        if p.is_open() { "Yes" } else { "No" }
    )
    .ct_unwrap();
    if print_sessions {
        for s in p.sessions() {
            print_session(s, fmt_time, fmt_date);
        }
    }
}
