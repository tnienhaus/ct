// Maybe make it a separate libary

use bunt::termcolor as tc;

static mut OUT: Option<tc::StandardStream> = None;
static mut ERR: Option<tc::StandardStream> = None;
static mut INITIALIZED: bool = false;

pub fn init(color_choice: tc::ColorChoice) {
    unsafe {
        OUT = Some(tc::StandardStream::stdout(color_choice));
        ERR = Some(tc::StandardStream::stderr(color_choice));
        INITIALIZED = true;
    }
}

fn initialized() -> bool {
    unsafe { INITIALIZED }
}

pub fn out() -> &'static mut tc::StandardStream {
    if !initialized() {
        // Just a simple println
        println!("The log module has not been initialized yet!");
        std::process::exit(-1);
    } else {
        unsafe { OUT.as_mut().unwrap() }
    }
}

pub fn err() -> &'static mut tc::StandardStream {
    if !initialized() {
        // Just a simple println
        println!("The log module has not been initialized yet!");
        std::process::exit(-1);
    } else {
        unsafe { ERR.as_mut().unwrap() }
    }
}
