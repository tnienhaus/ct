# ct

A command-line time tracking tool.

[![ct on crates.io](https://img.shields.io/crates/v/ct-tracker)](https://crates.io/crates/ct-tracker)

## What is `ct`

`ct` is, as it says in the top, a CLI time tracking tool, written in Rust, which tracks your time in a project based way.

## Planned usage

Because this has just started development, all if this can change.

### Start a new project

This will be done automatically when using `start` on a non-existant project.

```shell
$ ct create ct
Created new project 'ct'
```

### Tracking time of a project (likely only before v0.2.0)

Start tracking a new project:

```shell
$ ct start -p ct
Created new project 'ct'
Started tracking project 'ct'
```

Start tracking an existing project:

```shell
$ ct start -p ct
Time spent on project 'ct' already: 8h5m4s
Started tracking project 'ct'
```

Start tracking an existing project, while another one is being tracked:

```shell
$ ct start -p ct
Already tracking project 'npp'
Stopped tracking project 'npp'
Time spent on project 'npp' already: 8h5m4s
Time spent on project 'npp' in this session: 4h0m0s

Time spent on project 'ct' already: 8h5m4s
Started tracking project 'ct'
```

Start tracking a new project, while another one is being tracked:

```shell
$ ct start -p ct
Already tracking project 'npp'
Stopped tracking project 'npp'
Time spent on project 'npp' already: 8h5m4s
Time spent on project 'npp' in this session: 4h0m0s

Created new project 'ct'
Started tracking project 'ct'
```

Stop tracking a project:

```shell
$ ct stop -p # without a specific project
Time spent on project 'ct' already: 8h5m4s
Time spent on project 'ct' in this session: 4h0m0s
Stopped tracking project 'ct'
$ ct stop -p ct # with a specific project
Time spent on project 'ct' already: 8h5m4s
Time spent on project 'ct' in this session: 4h0m0s
Stopped tracking project 'ct'
```

### Tracking time of a task in a project (v0.2.0)

Start tracking new task:

```shell
$ ct start ct.tracking
Started tracking task 'tracking' in project 'ct'
```

Start tracking an existing task:

```shell
$ ct start ct.tracking
Time spent on 'ct.tracking' already: 8h5m4s
Started tracking task 'tracking' in project 'ct'
```

Stop tracking:

```shell
$ ct stop #without specific task
Stopped tracking task 'tracking' in project 'ct'
Time spent on 'ct.tracking' already: 8h5m4s
Time spent on 'ct.tracking' in last session: 4h0m0s
$ ct stop ct.tracking #with a specific task
Stopped tracking task 'tracking' in project 'ct'
Time spent on 'ct.tracking' already: 8h5m4s
Time spent on 'ct.tracking' in last session: 4h0m0s
```

Start tracking a task, while another one is currently going on:

```shell
$ ct start ct.export
Already tracking task 'tracking' in project 'ct'
Stopped tracking task 'tracking' in project 'ct'
Time spent on ct.tracking already: 8h5m4s
Time spent on ct.tracking in last session: 4h0m0s

Started tracking task 'export' in project 'ct'
```

## Roadmap/planned features

### 0.1.0

- Ability to track time in a JSON file
- For now only project based time tracking
- Basic reporting of time
- The ability to list already existing projects (for now only projects)
- The ability to clear all projects
- The ability to delete a specific project

### 0.2.0

- Task specific time tracking
- Ability to add/subtract time manually from a task/project
- Colored output, using termcolor for example
- Make `ct` configurable using a config.json/config.toml
  - Default save location
  - Parameter defaults (color, etc.)
  - Time format
- Somehow be able to be backwards compatible with 0.1.0
  - Upgrade command to upgrade old records
  - Legacy project based time tracking

### 0.3.0

- Exporting the time spent to markdown (and other formats)
- Report (and export) time per project in multiple formats, e.g.:
  - Time spent per day
- Ability to report time spent on a day

### Not planned currently (but nice to have)

- Encrypting time spent
- Backup option
  - Ability to backup tracked data and restore a backup
- Multiple users (?)
- VS Code extension (?)
  - UI integration
  - bound to workspace
    - Somehow integrate with git (see below)
- Git integration
  - Scan path from which ct is called, check if it's a git repository and use some metadata from there (project, branch, etc.)
- A GUI client, maybe using something like [libui](https://github.com/rust-native-ui/libui-rs)
